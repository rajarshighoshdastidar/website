# https://docs.docker.com/develop/develop-images/multistage-build/#stop-at-a-specific-build-stage
# https://docs.docker.com/compose/compose-file/#target


# https://docs.docker.com/engine/reference/builder/#understand-how-arg-and-from-interact
ARG NGINX_VERSION=1.17

# "nginx" stage
# depends on the "build" stage above
FROM nginx:${NGINX_VERSION}-alpine

COPY docker/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf

RUN mkdir -p /var/www/ryzencontroller/
WORKDIR /var/www/ryzencontroller/

COPY ./index.html ./
COPY ./fonts ./fonts
COPY ./images ./images
COPY ./scripts ./scripts
COPY ./css ./css

EXPOSE 5000