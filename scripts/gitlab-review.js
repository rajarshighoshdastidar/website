if (window.location.host !== "ryzencontroller.com") {
    var script = document.createElement('script');
    script.defer = true;
    script.setAttribute('data-project-id', '17440887');
    script.setAttribute('data-project-path', 'ryzen-controller-team/website');
    script.setAttribute('data-mr-url', 'https://gitlab.com');
    script.id = 'review-app-toolbar-script';
    script.src = 'https://gitlab.com/assets/webpack/visual_review_toolbar.js';
    document.getElementsByTagName('head').item(0).appendChild(script);
}
